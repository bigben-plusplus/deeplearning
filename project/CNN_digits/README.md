CNN_digits
================

A digits recognition task based on CNN.


## Introduction
----------------------------------
基于**DeeplearnToolbox**中的CNN实现进行digits数据集(8x8黑白图片, 包含0-9共10个数字类别)的分类与识别。
  
代码主要参照DeeplearnToolbox进行改写，同时借鉴了**MatConvNet**中的一些设计思想，使得代码更加优雅，同时层次也更加清楚，便于学习。

代码分别尝试了将训练数据看作一维和二维特征进行处理。
在第一种情况下，8x8图片数据被展成一个长度为64的向量。而在后一种情况下，图片数据直接作为一个8x8矩阵进行输入，这是当前利用CNN进行物体识别的主要模式。
仿真发现，两种情况下均能得到接近4%的错分类率（训练参数选择合适的情况下）。

## Setup
----------------------------------

Under Matlab command prompt, run
    addpath(genpath('CNN_digits'));

or, alternatively, run `cnn_digit_init.m`
	cnn_digit_init.m

We provide 2 demos located in `test`: cnn_digits8x8 and cnn_digits64x1

## Reference
----------------------------------

1. [DeepLearnToolbox](https://github.com/rasmusbergpalm/DeepLearnToolbox)
2. [MatConvNet](http://www.vlfeat.org/matconvnet)

----------------------------------
bigben@seu.edu.cn
2016/5/10


