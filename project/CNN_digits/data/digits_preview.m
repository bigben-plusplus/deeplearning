function digits_preview(n, varargin)
if ~exist('n', 'var'), n = 10; end

load digits feature label
N = size(feature, 2);
assert(size(feature, 2) == size(label, 2));

if (n > N), n = N; end

figure('Name','Digits 8x8 (Preview)', 'NumberTitle','off')
cm = repmat([1:255].', 1, 3)/256;
feature = feature/16;
for i = 1:n
    im = reshape(feature(:, i)*255, 8, 8).';
    imshow(imresize(im, 5, 'bilinear'), cm)
    title(sprintf('Index: %d, Label: %d', i, label(i)-1))
    pause(1.0)
end

end