function load_digits64x1(varargin)
load digits feature label

ratio = 0.8;
[M, N] = size(feature);
assert(M==64, 'xx')
x = double(reshape(feature/16.0, 64, 1, N)/255);
y = double(zeros(10, N));
for i = 1:N
    y(label(i), i) = 1;
end

[train, test] = random_split(x, y, ratio);
train_x = train.x;
train_y = train.y;
test_x = test.x;
test_y = test.y;

save digit64x1 train_x train_y test_x test_y
end

