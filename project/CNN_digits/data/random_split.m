function [train, test] = random_split(feature, label, ratio)
assert(size(feature, 3) == size(label, 2), 'xx')
if ~exist('ratio', 'var'), ratio = .8; end
assert(0.0 <= ratio <= 1.0, 'ratio should be in range [0.0, 1.0]')

N = size(feature, 3);
kk = randperm(N);
m = ceil(ratio*N);
train_selector = kk(1:m);
test_selector = kk(m+1:end);

train.x = feature(:, :, train_selector);
train.y = label(:, train_selector);
test.x = feature(:, :, test_selector);
test.y = label(:, test_selector);

end