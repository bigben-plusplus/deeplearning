function load_digits()
load digits.csv digits
feature = uint8(digits(:, 1:end-1)).';
label = uint8(digits(:,end)+1).';
save digits.mat feature label
end