function cnn_digits64x1(varargin)
load digit64x1

if ~exist('model', 'var')
opts.numepochs = 10;
opts.floatx = 'double';

opts.learningrate = .055;
opts.batchsize = 16;
opts.weightdecay = 1e-5;
opts.momentum = 0.5;
opts.plothandle = [];

opts.valid_x = test_x;
opts.valid_y = test_y;

cnn.name = 'cnn_digit64x1';
cnn.layers = {
    struct('type', 'input', 'name', 'input', 'inputsize', [64 1 1])
    struct('type', 'conv', 'name', 'conv1', 'outputmaps', 6, 'kernelsize', [3 1])
    struct('type', 'pool', 'name', 'pool1', 'method', 'mean', 'scale', [2, 1])
    struct('type', 'conv', 'name', 'conv2', 'outputmaps', 10, 'kernelsize',[4, 1])
    struct('type', 'pool', 'name', 'pool2', 'method', 'mean', 'scale', [4, 1])
    struct('type', 'output', 'name', 'output', 'outputsize', 10)
};
[cnn.activation, cnn.activation_gradient] = activation_factory('tanh');
cnn = cnnsetup(cnn, opts);
cnn = cnntrain(cnn, train_x, train_y, opts);
% save model
save cnn_digit64x1_model cnn opts

[mcr, ~] = cnntest(cnn, test_x, test_y);
fprintf('error rate on testset is %.4f%%\n', mcr*100)
end
