function cnn_digits8x8(varargin)
load digit8x8

opts.numepochs = 10;
opts.floatx = 'double';

opts.learningrate = .095;
opts.batchsize = 8;
opts.weightdecay = 1e-5;
opts.momentum = 0.5;
opts.plothandle = [];

opts.valid_x = test_x;
opts.valid_y = test_y;

cnn.name = 'cnn_digi8x8';
cnn.layers = {
    struct('type', 'input', 'name', 'input', 'inputsize', [8 8 1])
    struct('type', 'conv', 'name', 'conv1', 'outputmaps', 6, 'kernelsize', [3 3])
    struct('type', 'pool', 'name', 'pool1', 'scale', [2, 2])
    struct('type', 'conv', 'name', 'conv2', 'outputmaps', 10, 'kernelsize',[2, 2])
    %struct('type', 'pool', 'name', 'pool2', 'scale', [2, 2])
    struct('type', 'output', 'name', 'output', 'outputsize', 10)
};
[cnn.activation, cnn.activation_gradient] = activation_factory('tanh');

cnn = cnnsetup(cnn, opts);
cnn = cnntrain(cnn, train_x, train_y, opts);

% save model
save cnn_digit8x8_model cnn opts

[mcr, ~] = cnntest(cnn, test_x, test_y);
fprintf('error rate on testset is %.4f%%\n', mcr*100)
end