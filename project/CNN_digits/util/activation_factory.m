function [fobj, fgradobj] = activation_factory(act)
% Reference: https://en.wikipedia.org/wiki/Activation_function
switch act
    case 'sigmoid'
        fobj = @(x)[1.0 ./ (1.0+exp(-x))];
        fgradobj = @(x)[1.0 ./ (1.0+exp(-x)) ./ (1.0+exp(x))];
    case 'tanh'
        fobj = @(x)[tanh(x)];
        fgradobj = @(x)[1-tanh(x).^2];
    case 'tanh_opt'
        fobj = @(x)[1.7159 * tanh(2.0/3.0*x)];
        fgradobj = @(x)[1.7159 * 2.0/3.0 * 1-tanh(2.0/3.0*x).^2];
    case 'relu'
        fobj = @(x)[max(0, x)];
        fgradobj = @(x)[double(x>0)];
    case 'arctan'
        fobj = @(x)[arctan(x)];
        fgradobj = @(x)[1./(1+x.^2)];
    case 'softsign'
        fobj = @(x)[x ./ (1+abs(x))];
        fgradobj = @(x)[1./(1+abs(x)).^2];
  	case 'gaussian'
        fobj = @(x)[exp(-x.^2)];
        fgradobj = @(x)[-2*x.*exp(-x.^2)];
    otherwise
        fobj = @(x)[x];
        fgradobj = @(x)[1];
end
end
