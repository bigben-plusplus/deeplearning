function [opts, args] = argparse(opts, args, varargin)
if ~isstruct(opts), error('OPTS must be a structure') ; end
if ~iscell(args), args = {args} ; end

recursive = true ;
if numel(varargin) == 1
  if strcmpi(varargin{1}, 'nonrecursive') ;
    recursive = false ;
  else
    error('Unknown option specified.') ;
  end
end
if numel(varargin) > 1
  error('There can be at most one option.') ;
end

% convert ARGS into a structure
ai = 1 ;
params = {} ;
values = {} ;
while ai <= length(args)
  if ischar(args{ai})
    params{end+1} = args{ai} ; ai = ai + 1 ;
    values{end+1} = args{ai} ; ai = ai + 1 ;
  elseif isstruct(args{ai}) ;
    params = horzcat(params, fieldnames(args{ai})') ;
    values = horzcat(values, struct2cell(args{ai})') ;
    ai = ai + 1 ;
  else
    error('Expected either a param-value pair or a structure') ;
  end
end
args = {} ;

% copy parameters in the opts structure, recursively
for i = 1:numel(params)
  field = params{i} ;
  if ~isfield(opts, field)
    field = findfield(opts, field) ;
  end
  if ~isempty(field)
    % The parameter was found in OPTS
    
    if isstruct(opts.(field))
      % The parameter has a struct value (in OPTS)
      if ~isstruct(values{i})
        error('Cannot assign a non-struct value to the struct parameter ''%s''.', ...
          field) ;
      end
      if recursive && numel(fieldnames(opts.(field))) > 0
        % Process the struct value recursively
        if nargout > 1
          [opts.(field), rest] = vl_argparse(opts.(field), values{i}) ;
          args = horzcat(args, {field, cell2struct(rest(2:2:end), rest(1:2:end), 2)}) ;
        else
          opts.(field) = vl_argparse(opts.(field), values{i}) ;
        end
      else
        % Copy the struct value as is
        opts.(field) = values{i} ;
      end
    else
      % The parameter does not have a struct value (in OPTS)
      % Copy as is
      opts.(field) = values{i} ;
    end
  else
    % The parameter was *not* found in OPTS
    if nargout <= 1
      error('Uknown parameter ''%s''', params{i}) ;
    else
      args = horzcat(args, {params{i}, values{i}}) ;
    end
  end
end

function field = findfield(opts, field)
fields=fieldnames(opts) ;
i=find(strcmpi(fields, field)) ;
if ~isempty(i)
  field=fields{i} ;
else
  field=[] ;
end
