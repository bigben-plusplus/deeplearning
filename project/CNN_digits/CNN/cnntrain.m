function net = cnntrain(net, x, y, opts)
    m = size(x, 3);
    numbatches = m / opts.batchsize;
    assert(rem(numbatches, 1) == 0, 'Error: numbatches not integer');
    net.rL = [];
    if (isempty(opts.plothandle))
        opts.plothandle = figure('Name', ['Train ' net.name], 'NumberTitle', 'off');
        xlabel('Iteration(s)'), ylabel('Loss'), grid on;
    end
    
    alpha = 0.99;
    
    for iter = 1 : opts.numepochs
        opts.learningrate = opts.learningrate - opts.weightdecay;
        fprintf('epoch %d/%d\n', iter, opts.numepochs);
        if (iter > 10), opts.momentum = 0.9; end
        
        tic;
        kk = randperm(m);
        for l = 1 : numbatches
            batch_x = x(:, :, kk((l - 1) * opts.batchsize + 1 : l * opts.batchsize));
            batch_y = y(:,    kk((l - 1) * opts.batchsize + 1 : l * opts.batchsize));

            net = cnnff(net, batch_x);
            net = cnnbp(net, batch_y);
            net = cnnupdate(net, opts);
            
            if isempty(net.rL), net.rL(1) = net.Loss; end
            % smooth the rL
            net.rL(end + 1) = alpha * net.rL(end) + (1-alpha) * net.Loss;
        end
        toc;

        [err, ~] = cnntest(net, opts.valid_x, opts.valid_y);
        fprintf('MisClassfied Rate (validation) is %.4f%% (learning rate: %f, momentum: %f)\n', ...
            err*100.0, opts.learningrate, opts.momentum);
        figure(opts.plothandle), cla
        plot(net.rL, 'r')
        xlabel('Iteration(s)'), ylabel('Loss'), grid on
        % title(sprintf('Epoch: %d', iter))
        drawnow
        opts.plothandle = gcf;
    end    
end
