function net = cnnbp(net, y)
    n = numel(net.layers) - 1;

    %   error
    net.err = net.output - y;
    %  loss function
    net.Loss = 1/2* sum(net.err(:) .^ 2) / size(net.err, 2);

    % backprop deltas
    net.od = net.err .* net.activation_gradient(net.output);   %  output delta
    % feature vector delta
    net.fvd = (net.ffW' * net.od);
    % only conv layers has activation function
    if strcmpi(net.layers{n}.type, 'conv'), net.fvd = net.fvd .* net.activation_gradient(net.fv); end

    %  reshape feature vector deltas into output map style
    sa = size(net.layers{n}.a{1});
    fvnum = sa(1) * sa(2);
    for j = 1 : numel(net.layers{n}.a)
         net.layers{n}.d{j} = reshape(net.fvd(((j - 1) * fvnum + 1) : j * fvnum, :), sa(1), sa(2), sa(3));
    end

    for l = (n - 1) : -1 : 1
        switch lower(net.layers{l}.type)
            case 'conv'
                for j = 1 : numel(net.layers{l}.a)
                    net.layers{l}.d{j} = net.activation_gradient(net.layers{l}.a{j}) .* ...
                        (expand(net.layers{l + 1}.d{j}, [net.layers{l + 1}.scale(1) net.layers{l + 1}.scale(2) 1]));             
                end
            case 'pool'
                for i = 1 : numel(net.layers{l}.a)
                    z = zeros(size(net.layers{l}.a{1}));
                    for j = 1 : numel(net.layers{l + 1}.a)
                         z = z + convn(net.layers{l + 1}.d{j}, rot180(net.layers{l + 1}.k{i}{j}), 'full');
                    end
                    net.layers{l}.d{i} = z;
                end
        end
    end

    % calculate gradients
    for l = 2 : n
        if strcmpi(net.layers{l}.type, 'conv')
            for j = 1 : numel(net.layers{l}.a)
                for i = 1 : numel(net.layers{l - 1}.a)
                    net.layers{l}.dk{i}{j} = ...
                        convn(flipall(net.layers{l - 1}.a{i}), net.layers{l}.d{j}, 'valid') ...
                        / size(net.layers{l}.d{j}, 3);
                end
                net.layers{l}.db{j} = sum(net.layers{l}.d{j}(:)) / size(net.layers{l}.d{j}, 3);
            end
        end
    end
    net.dffW = net.od * (net.fv)' / size(net.od, 2);
    net.dffb = mean(net.od, 2);

    function X = rot180(X)
        X = flipdim(flipdim(X, 1), 2);
    end

    function X = flipall(X)
        for dim = 1:ndims(X)
            X = flipdim(X, dim);
        end
    end

    function B = expand(A, S)
        SA = size(A);
        T = cell(length(SA), 1);
        for ii = length(SA) : -1 : 1
            H = zeros(SA(ii) * S(ii), 1);       %  One index vector into A for each dim.
            H(1 : S(ii) : SA(ii) * S(ii)) = 1;	%  Put ones in correct places.
            T{ii} = cumsum(H);                  %  Cumsumming creates the correct order.
        end
        B = A(T{:});
    end
end
