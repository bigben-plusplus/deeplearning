function net = cnnupdate(net, opts)
% update weight with gradient   
for l = 2 : numel(net.layers)
    if strcmpi(net.layers{l}.type, 'conv')
        for j = 1 : numel(net.layers{l}.a)
            for i = 1 : numel(net.layers{l - 1}.a)
                net.layers{l}.vk{i}{j} = ...
                    opts.momentum * net.layers{l}.vk{i}{j} - opts.learningrate * net.layers{l}.dk{i}{j};
                net.layers{l}.k{i}{j} = ...
                    net.layers{l}.k{i}{j} + net.layers{l}.vk{i}{j};
            end
            net.layers{l}.vb{j} = ...
                opts.momentum * net.layers{l}.vb{j} - opts.learningrate * net.layers{l}.db{j};
            net.layers{l}.b{j} = net.layers{l}.b{j} + net.layers{l}.vb{j}; 
        end
    end
end

net.vffW = opts.momentum * net.vffW - opts.learningrate * net.dffW;
net.vffb = opts.momentum * net.vffb - opts.learningrate * net.dffb;

net.ffW = net.ffW + net.vffW;
net.ffb = net.ffb + net.vffb;

end