function [err, bad] = cnntest(net, x, y)
    %  feedforward
    net = cnnff(net, x);
    [~, h] = max(net.output);
    [~, a] = max(y);
    bad = find(h ~= a);

    err = numel(bad) / size(y, 2);
end
