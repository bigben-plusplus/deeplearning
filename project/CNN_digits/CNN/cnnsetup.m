function net = cnnsetup(net, opts)
    numlayers = numel(net.layers);
    for l = 1 : numlayers
        switch lower(net.layers{l}.type)
            case 'input'
                assert(l == 1, 'The 1st layer must be input')
                inputsize = net.layers{l}.inputsize;
                mapsize = inputsize(1:2);
                inputmaps = inputsize(3);
                % fprintf('inputsize: [%s]\n', num2str(inputsize))
            case 'pool'
                assert(strcmpi(net.layers{l-1}.type, 'conv'), 'The previous layer must be conv')
                mapsize = mapsize ./ net.layers{l}.scale;
                assert(all(floor(mapsize)==mapsize), ...
                    ['Layer ' num2str(l) ' size must be integer. Actual: ' num2str(mapsize)]);
                for j = 1 : inputmaps, net.layers{l}.b{j} = 0; end
                for j = 1 : net.layers{l-1}.outputmaps, net.layers{l}.a{j} = zeros(mapsize); end
            case 'conv'
                mapsize = mapsize - net.layers{l}.kernelsize + 1;
                fan_out = net.layers{l}.outputmaps * prod(net.layers{l}.kernelsize);
                fan_in = inputmaps * prod(net.layers{l}.kernelsize);
                for j = 1 : net.layers{l}.outputmaps  %  output map                
                    for i = 1 : inputmaps  %  input map
                        net.layers{l}.k{i}{j} = 1*(rand(net.layers{l}.kernelsize) - 0.5) * 2 ...
                            * sqrt(6 / (fan_in + fan_out));
                        net.layers{l}.vk{i}{j} = zeros(size(net.layers{l}.k{i}{j}));
                    end
                    net.layers{l}.b{j} = 0;
                    net.layers{l}.vb{j} = 0;
                    net.layers{l}.a{j} = zeros(mapsize);
                end
                inputmaps = net.layers{l}.outputmaps;
            case 'output'
                assert(l == numlayers, 'The last layer must be output')
                fan_in = prod(mapsize) * inputmaps;
                onum = net.layers{l}.outputsize;
                fan_out = onum(1);
                net.ffb = zeros(onum, 1);                
                net.ffW = (rand(fan_out, fan_in) - 0.5) * 2 * sqrt(6 / (fan_out + fan_in));
                net.vffb = zeros(size(net.ffb));
                net.vffW = zeros(size(net.ffW));
        end
    end
end
