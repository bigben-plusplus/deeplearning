function net = cnnff(net, x)
    n = numel(net.layers);
    net.layers{1}.a{1} = x;
    inputmaps = 1;
    
    for l = 2 : n   %  for each layer
        switch lower(net.layers{l}.type)
            case 'conv'
                %  !!below can probably be handled by insane matrix operations
                for j = 1 : net.layers{l}.outputmaps   %  for each output map
                    z = zeros(size(net.layers{l - 1}.a{1}) - [net.layers{l}.kernelsize - 1 0]);                
                    for i = 1 : inputmaps   %  for each input map
                        %  convolve with corresponding kernel and add to temp output map
                        z = z + convn(net.layers{l - 1}.a{i}, net.layers{l}.k{i}{j}, 'valid');
                    end
                    %  add bias, pass through nonlinearity
                    net.layers{l}.a{j} = net.activation(z + net.layers{l}.b{j});
                end
                %  set number of input maps to this layers number of outputmaps
                inputmaps = net.layers{l}.outputmaps;
            case 'pool'
                %  downsample
                for j = 1 : inputmaps
                    z = convn(net.layers{l - 1}.a{j}, ones(net.layers{l}.scale) / (prod(net.layers{l}.scale)), 'valid');
                    net.layers{l}.a{j} = z(1 : net.layers{l}.scale(1) : end, 1 : net.layers{l}.scale(2) : end, :);
                end
            case 'output'
                %  concatenate all end layer feature maps into vector
                net.fv = [];
                prev_layer = net.layers{l-1};
                sa = size(prev_layer.a{1});
                for j = 1 : numel(prev_layer.a)        
                    net.fv = [net.fv; reshape(prev_layer.a{j}, sa(1) * sa(2), sa(3))];
                end
                %  feedforward into output perceptrons
                net.output = net.activation(net.ffW * net.fv + repmat(net.ffb, 1, size(net.fv, 2)));
        end
    end
end
